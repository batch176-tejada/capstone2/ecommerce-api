const jwt = require('jsonwebtoken');

const orders = "productOrdersbyUser";

module.exports.createAccessToken = (user) => {

	console.log(user);

	const person ={
					id: user._id,
					email: user.email,
					isAdmin: user.isAdmin
	};

	return jwt.sign(person, orders, {})

};
module.exports.verify = (req,res, next) =>{

	let token = req.headers.authorization;
	//console.log(token);

	if (typeof token === "undefined") {
		return res.send({auth: "Failed. No token"});
	}
	else {
		console.log(token);
		token = token.slice(7, token.length)
		jwt.verify(token, orders, function(err,decodedToken){
				if (err) {
					return res.send({
						auth: "Failed",
						message: err.message
					})
				} 
				else {
					console.log(decodedToken);
					req.user = decodedToken

					next();
				}
		
		})


	}

}

//verify admin
	module.exports.verifyAdmin = (req,res,next) =>{
			const admin = req.user;

				if (admin.isAdmin) {
					next()
				} 
				else {
					return res.send({
						auth:"Failed",
						message: "Action Cannot be Granted"
					})
				}
	}