//[SECTION]
	const User = require('../models/User');
	const Product = require('../models/Product');
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv');
	const auth = require('../auth.js');

//[SECTION]Environmental Variables Setup
	dotenv.config();
	const naCl = parseInt(process.env.SALT);

//[SECTION]create
	module.exports.register = (userData) => {
		let email = userData.email;
		let passW = userData.password;
		let costumer = new User({
				email: email,
				password: bcrypt.hashSync(passW,naCl)
		});
		return costumer.save().then((logged,denied) => {
			if (logged) {
				return logged;

			} 
			else {
				return 'Failed to Register account User';
			};
		});
	};

//[SECTION]retrieve all
	module.exports.getAllUsers = () => {
		return User.find({}).then(result =>{
			return result
		})
	} 

//[SECTION]Authentication
	module.exports.userLogIn = (data) =>{
		return User.findOne({email: data.email}).then(result => {
			if (result == null) {
				return false
			} 
			else {
				const isPassWordRight = bcrypt.compareSync(data.password,result.password)

					if (isPassWordRight) {
						return {accessToken: auth.createAccessToken(result.toObject())}
					}
					else {
						return false
					};
			};
		});
	};

	module.exports.order = async (req,res) => {

			if (req.user.Admin) {
				res.send({message: "Action Forbidden"})
			} 
			
			let isUserUpdated = await User.findById(req.user.id).then(user =>{
				let newOrder = {
						productId: req.body.productId,
						quantity: req.body.quantity,
						price: req.body.price,
						totalCost: req.body.quantity * req.body.price
					} 
				

			user.orders.push(newOrder);
			return user.save().then(user => true).catch(err => err.message)
				if(isUserUpdated !== true){
					return res.send({message: isUserUpdated})
				}	
			});

				let isProductUpdated = await Product.findById(req.body.productId).then(product => {
						let purchaser = {
								orderId: req.user.id
						}

						product.purchasers.push(purchaser);
							return product.save().then(product => true).catch(err => err.message)


						if(isProductUpdated !== true) {
								return res.send({ message: isProductUpdated })
						}

			})

			if(isUserUpdated && isProductUpdated) {
					return res.send({ message: "Successfully purchase the Item" })
			}

	}
