//[SECTION]
	const Product = require('../models/Product');
	const auth = require('../auth.js');

//[SECTION]create
	module.exports.register = (prodData) => {
		let pName = prodData.name;
		let pDescription = prodData.description;
		let pPrice = prodData.price;
	
		let newItem = new Product({
				name: pName,
				description: pDescription,
				price: pPrice
		});
		return newItem.save().then((input,denied) => {
			if (input) {
				return input;

			} 
			else {
				return 'Failed to Register Item';
			};
		});
	};

//[SECTION]retrieve all
	module.exports.getAllItems = () => {
		return Product.find({}).then(presult => {
			return presult;
		})
	} 

//[SECTION]retrieve specific ONLY
	module.exports.getspecProduct = (specProd) => {
		return Product.findById(specProd).then((display, err) => {

			if (display) {
				console.log(display);
				return {message: `Data Retrieve: ${display.name} @ ${display.price}`};
			} 
			else {
				return {message:`Error in Retrieving DATA`};
			};
		});
	};


//[SECTIOn]Update specific 
	module.exports.revisedProduct = (productId,revisedProduct) => {
		let updatedProduct = {
			name: revisedProduct.name,
			description: revisedProduct.description,
			price: revisedProduct.price
		}

		return Product.findByIdAndUpdate(productId, updatedProduct).then((modify,err) => {
				if(error){
				    return false;
				} 
				else {
				    return {message:`DATA change ${modify.body}`};
				}
				})
				.catch(error => error)
			}

//[SECTIOn]Archive specific 
	module.exports.archiveProduct = (productId) => {
		let updateActiveField = {
		isActive: false
	};

	return Product.findByIdAndUpdate(productId, updateActiveField).then((product, error) => {

		if(error){
			return false;
		} else {
			return true;
		}
	}).catch(error => error)
}


	