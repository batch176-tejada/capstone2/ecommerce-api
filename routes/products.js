//dependencies
	const express =	require('express');
	const controller = require('../controllers/products');
	const auth = require('../auth');

//routing component
	const route = express.Router();
	const {verify, verifyAdmin} = auth;

//Expose Route System
	module.exports = route;

//rout-post
	route.post('/pregister',verify, verifyAdmin,(req,res) => {
		const item = req.body;
		console.log(item);
		controller.register(item).then(buy =>{
			res.send(buy);
		})
	});

//route-get-all
	route.get('/allProduct',(req, res) =>{
	controller.getAllItems().then(viewProducts=> res.send(viewProducts))
	});



//rout-retrieve specific item
	route.get('/specProduct/:id',(req, res) =>{
		let xProd = req.params.id;
		controller.getspecProduct(xProd).then(sProductId => res.send(sProductId));
	});


//rout-update specific item
	route.put("/updateProduct/:id",verify, verifyAdmin,(req, res) =>{
		let updateProd = req.params.id;
		let updatePBody = req.body;
		controller.revisedProduct(updateProd, updatePBody).then(updatedProductId => res.send(updatedProductId));
	});


//Archiving a item 
	route.put('/archive/:id', verify, verifyAdmin, (req, res) => {
		let archiveProduct = req.params.id;
		controller.archiveProduct(archiveProduct).then(result => res.send(result));
	})

