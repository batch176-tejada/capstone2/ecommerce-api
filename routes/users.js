//dependencies
	const express =	require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth')

//routing component
	const route = express.Router();

//Expose Route System
	module.exports = route;

//rout-post
	route.post('/uregister',(req,res) => {
		const buyer = req.body;
		console.log(buyer);
		controller.register(buyer).then(order =>{
			res.send(order);
		})
	});

//rout-get-all-user
	route.get('/registered',(req, res) =>{
	controller.getAllUsers().then(allCustomer => res.send(allCustomer))
})

//route-user-auth
	route.post('/login',(req,res) =>{
		const accessOne = req.body;
		controller.userLogIn(accessOne).then(result => res.send(result));
	})

//Registered User Order Creation
	route.post('/order', auth.verify, controller.order);