//[SECTION] Dependencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const cors = require('cors');
  	const userRoutes = require('./routes/users');
  	const prodRoutes = require('./routes/products');



//[SECTION] Environment Setup
	dotenv.config();
	let letsGo = process.env.SERVER;
	const haven = process.env.PORT;			 

//[SECTION] Server Setup
	const app = express();
	app.use(express.json());
	app.use(cors());


//[SECTION] Database Connection 
	mongoose.connect(letsGo, {
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	)
		const connectStatus = mongoose.connection;
		connectStatus.once('open', () => console.log(`Loading.........Database Connected`));

//[SECTION] Backend Routes 
	app.use('/users',userRoutes);
	app.use('/product',prodRoutes);

	
	

//[SECTION] Server Gateway Response

	app.get('/',(req,res)=>{
		res.send("What's Your Order? Lets go get it...")
	});
	app.listen(haven, () => { 
		console.log(`API is hosted in port ${haven}`); 
	});