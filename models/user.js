//[SECTION] Modules and Dependencies
	const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint
	const userBluprint = new mongoose.Schema({
		email: {
			type: String,
				required: [true, 'Your Email Address is Required']
		},
		password: {
			type: String,
				required: [true, 'Password is Required']
		},
		isAdmin: {
			type: Boolean,
				default: false
		},
		orders: [
			{	
				productId:{
					type: String,
					required: [true, 'Item is Required']
				},
				quantity:{
					type: Number,
					required: true,
					min: [1, 'Quantity can not be less then 1.']
				},
				price:{
					type:Number
				},	
				totalCost: {
					type: Number,
					default:0
				},
				purchaseOn:{
					type: Date,
					default: new Date()
				}

			}
		]


	});


//[SECTION] Model
	module.exports = mongoose.model('User',userBluprint);	