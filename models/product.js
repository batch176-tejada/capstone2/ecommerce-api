//[SECTION] Modules and Dependencies
	const mongoose = require('mongoose');

//[SECTION] Schema/Blueprint
	let productBluprint = new mongoose.Schema({
		name: {
			type: String,
			required: [true, ' Name of the Product is Required']
		},
		description: {
			type: String,
			required:[true, 'Description needed']
		},
		price: {
			type: Number,
			required: [true,' Price of the Product is Required']
		},
		isActive: {
			type: Boolean,
			default: true
		},
		createdOn: {
			type: Date,
			default: new Date()
		},
		purchasers:[
			{
				orderId: {
					type: String,
					required: [true,'Order Identity is Required']
				}
			}
		]
	})


//[SECTION] Model
	module.exports = mongoose.model('Product',productBluprint);	